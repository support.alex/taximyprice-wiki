# Описание

**Главная страница** - это витрина сайта. На ней расположены 2 основных рабочих инструмента для пользователей.

![](https://cloclo20.datacloudmail.ru/weblink/view/7v8d/RdBBKbqiS?etag=DA6A642C291799300B0EBBB502561DF2521C06BF&key=f43d0970bf34358cc269e5dffbb080beb876cfd7)

1. 2 ссылки/кнопки;
1. Шаги оформления завки для пассажира;
1. Биржа с размещенными завками для водителя.

## Cсылки/кнопки <links>

1. **"Place your ride in 4 steps"** - переход на главную страницу сайта с шагами создания завки;
2. **"Take an order"** - переход на страницу биржи `/main-driver` для водителя.

## Шаги оформления завки <ordercreate>

Модуль предназначен для создания поездки пассажиром. Описание работы на этой [странице]().

## Биржа <exchange>

Модуль предназначен покупки размещенной завки водителем. Описание работы на этой [странице]().

На главной странице выводится до 10 заявок на бирже при загрузке страницы. Нажатием на кнопку "More" можно открыть большее число завок размещенных на бирже.