# Карта сайта

## Главная страницы <homepage>

[Главная страница](http://taximyprice.com)

## Страница биржи <homepage>

[Отдельная страница биржи](http://taximyprice.com/main-driver)

## Регистрация пользователя <regpages>

1. [Авторизация](http://taximyprice.com/login)
2. [Регистрация пассажира](http://taximyprice.com/register)
3. [Подтверждение регистрации](http://taximyprice.com/register) *доступна после заполнения регистрационных данных*
4. [Подтверждение почты](http://taximyprice.com/check-email)
5. [Страница выбора офиса](http://taximyprice.com/choose-office)
6. [Регистрация независимого водителя](http://taximyprice.com/register-independent-driver)
7. [Регистрация такси компании](http://taximyprice.com/) *"в разработке"*
8. [Регистрация водителя такси компании](http://taximyprice.com/) *"в разработке"*
9. [Регистрация менеджера такси компании](http://taximyprice.com/) *"в разработке"*


## Размещение заявки на биржу <requests>
1. [На главной](http://taximyprice.com/) *
2. [В офисе пассажира](http://taximyprice.com/office-passenger) *
3. [Проверка и подтверждение завки](http://taximyprice.com/confirm/request_id/425) **
4. [Оплата завки](http://taximyprice.com/payment/request_id/425) **
5. [Редактирование завки](http://taximyprice.com/edit/request_id/425) **

*3 шага на одном url <br>
**id заявки в конце ссылки меняется


## Офис пассажира <passenger>
1. [Главная, создание заявки](http://taximyprice.com/office-passenger) (3 шага создания заявки)
1. [Open orders](http://taximyprice.com/office-passenger/open-orders)
1. [All orders](http://taximyprice.com/office-passenger/all-orders)
1. Профиль пассажира на всех страницах офиса в модальном окне.

## Финансовый офис <finoffice>
1. [Ввод денег в системму](http://taximyprice.com/financial-office/add) *в разработке*
1. [Вывод денег из системы](http://taximyprice.com/financial-office/withdraw) *в разработке*
1. [История финансов](http://taximyprice.com/financial-office/history) 
	1. [TMP market](http://taximyprice.com/financial-office/history) 
	1. [Transactions](http://taximyprice.com/financial-office/history/addition) 

## Офис независимого водителя <driver>
1. [Find order](http://taximyprice.com/office-driver)
1. [Open orders](http://taximyprice.com/office-driver/open)
1. [All orders](http://taximyprice.com/office-driver/history)
1. Профиль независимого водителя на всех страницах офиса в модальном окне.

## Офис партнера <parthners>

1. [Список партнерских программ](http://taximyprice.com/parthner-programms)
1. [Партнерские программы - офис](http://taximyprice.com/parthner-office)

##  Страницы помощи <helppages>
1. [How it work?](http://taximyprice.com/help)
2. [FAQs](http://taximyprice.com/faqs)
3. [Guides](http://taximyprice.com/guides)

Для каждой страницы есть вкладка для пассажира и для водителя.

## Информационные страницы <infopages>
1. [О нас](http://taximyprice.com/about)
2. [Profit](http://taximyprice.com/profit) <br>
	Вкладки:
	1. Profit passenger
		1. Business group
		2. Pets
		3. Add services
		4. Spec. Drivers
	2. Profit drivers
		1. Weekly profit
		2. 5000 profit
		3. Extra
		4. Extra money
3. [Блог](http://taximyprice.com/blog) <br>
	Категории/подкатекогрии:
	1. [TMP Life](http://taximyprice.com/blog/tmp-life)
		1. [TMP News](http://taximyprice.com/blog/tmp-news)
		1. [International money and people](http://taximyprice.com/blog/money-and-people)
		1. [GEO TMP info](http://taximyprice.com/blog/geo-tmp-info)
	1. [For Passenger](http://taximyprice.com/blog/for-passenger)
		1. [News for passangers](http://taximyprice.com/blog/news-for-passenger)
		1. [Local money](http://taximyprice.com/blog/local-money)
		1. [Local info for passanger](http://taximyprice.com/blog/local-info-for-passenger)
	1. [For Driver](http://taximyprice.com/blog/for-driver)
		1. [News for drivers](http://taximyprice.com/blog/news-for-driver)
		1. [Local money and driver](http://taximyprice.com/blog/local-money-and-driver)
		1. [Local info for driver](http://taximyprice.com/blog/local-info-and-driver)

## Внутренние сообщения (Notifications) <notifications>
1. [Все сообщения](http://taximyprice.com/notifications)
1. [TMP Market](http://taximyprice.com/notifications/market)
1. [TMP Store](http://taximyprice.com/notifications/store)
1. [My Wallet](http://taximyprice.com/notifications/payment)
1. [Basket](http://taximyprice.com/notifications/deleted)

## Магазин <store>
[Страницы магазина](http://taximyprice.com/store) *в разработке*

